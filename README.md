
This project was built on top of the Spring Boot for IBM Cloud template to demonstrate a Spring Boot REST API.

## Requirements
Maven
Java

## Features:
Exposed REST API Endpoint
Validation for request payload
jUnit tests for code coverage
Spring Actuator for health checks
Springfox Swagger2 for endpoint documentation
Spring Security for basic authentication

## Run Locally:
> git clone {repo}
> mvn package
> deploy to your favorite application server

## Demo
[https://allstate-sample-app.mybluemix.net/swagger-ui.html#/post-resource-controller/processRequestUsingPOST](https://allstate-sample-app.mybluemix.net/swagger-ui.html#/post-resource-controller/processRequestUsingPOST)
