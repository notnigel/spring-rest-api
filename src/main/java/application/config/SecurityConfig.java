package application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/*
    This class defines the security constraints for the application.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    ApplicationContext context;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //Retrieve authentication properties from app context
        auth
                .inMemoryAuthentication()
                .withUser(context.getEnvironment().getProperty("application.authentication.user"))
                .password(context.getEnvironment().getProperty("application.authentication.password"))
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //authenticate all the http requests
        http
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();

        //Disable CSFR. No need for it since there isn't any sensitive information here
        http
                .csrf()
                .disable();
    }
}