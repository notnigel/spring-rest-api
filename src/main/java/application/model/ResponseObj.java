package application.model;

public class ResponseObj {

    private String status;
    private String details;

    public ResponseObj(){};

    public ResponseObj(String status, String details) {
        this.status = status;
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

}