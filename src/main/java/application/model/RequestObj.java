package application.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "requestId",
        "emailAddress",
        "people"
})
public class RequestObj {

    @JsonCreator
    public RequestObj(@JsonProperty("requestId") String requestId,
                      @JsonProperty("emailAddress") String emailAddress,
                      @JsonProperty("people") List<Person> people) {
        this.requestId = requestId;
        this.emailAddress = emailAddress;
        this.people = people;
    }

    @JsonProperty("requestId")
    @NotEmpty(message = "requestId name cannot be empty")
    private String requestId;

    @JsonProperty("emailAddress")
    @NotEmpty(message = "emailAddress cannot be empty")
    @Email(message = "emailAddress must be a valid")
    private String emailAddress;

    @JsonProperty("people")
    @Valid
    @NotEmpty(message = "people object must contain at least 1 element")
    private List<Person> people = null;

    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("emailAddress")
    public String getEmailAddress() {
        return emailAddress;
    }

    @JsonProperty("emailAddress")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @JsonProperty("people")
    public List<Person> getPeople() {
        return people;
    }

    @JsonProperty("people")
    public void setPeople(List<Person> people) {
        this.people = people;
    }

}
