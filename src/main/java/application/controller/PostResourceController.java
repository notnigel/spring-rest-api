package application.controller;

import application.model.RequestObj;
import application.model.ResponseObj;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/apis")
public class PostResourceController {

    //Return the number of people processed and the associated email address
    @PostMapping("/processRequest")
    public ResponseEntity<ResponseObj> processRequest(
            @RequestBody @Valid RequestObj requestObj) {
        return ResponseEntity.ok(new ResponseObj("ok", buildSuccessDetails(requestObj)));
    }

    //Handle exceptions for a bad requests if validation does not pass
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ResponseObj> badRequest(MethodArgumentNotValidException  e) {
        return ResponseEntity.badRequest().body(
                new ResponseObj("fail", e.getBindingResult().getFieldError().getDefaultMessage())
        );
    }

    //Handle exceptions for invalid JSON
    @ExceptionHandler({HttpMediaTypeNotSupportedException.class, HttpMessageNotReadableException.class})
    public ResponseEntity<ResponseObj> invalidJSON(HttpMediaTypeNotSupportedException  e) {
        return ResponseEntity.badRequest().body(new ResponseObj("fail", e.getMessage()));
    }

    //Build success details response
    private String buildSuccessDetails(RequestObj requestObj) {
        return "Processed " + requestObj.getPeople().size() + " people for " + requestObj.getEmailAddress();
    }
}
