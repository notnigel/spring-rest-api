package application.controller;

import static org.junit.Assert.assertEquals;

import application.model.Person;
import application.model.RequestObj;
import application.model.ResponseObj;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResourceAccessException;

import java.util.Arrays;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PostRequestControllerTest {

    @Autowired
    private TestRestTemplate server;

    @LocalServerPort
    private int port;

    private String endpoint;

    @Before
    public void setup(){
        endpoint = "http://localhost:" + port + "/apis/processRequest";
    }

    //Test Basic Auth
    @Test(expected = ResourceAccessException.class)
    public void basicAuthTest() {
        ResponseEntity<ResponseObj> response = server
                .postForEntity(endpoint, buildRequestBody(), ResponseObj.class);
    }

    //Successful request
    @Test
    public void processRequestTest() throws Exception {

        ResponseEntity<ResponseObj> response = server
                .withBasicAuth("applicationUser", "applicationPassword")
                .postForEntity(endpoint, buildRequestBody(), ResponseObj.class);

        HttpStatus status = response.getStatusCode();
        assertEquals("Invalid response from server : " + response, HttpStatus.OK, status);
    }

    //Bad Request
    @Test
    public void processRequestBadRequestTest() {

        ResponseEntity<ResponseObj> response = server
                .withBasicAuth("applicationUser", "applicationPassword")
                .postForEntity(endpoint, buildBadRequestBody(), ResponseObj.class);

        HttpStatus status = response.getStatusCode();
        assertEquals("Invalid response from server : " + response, HttpStatus.BAD_REQUEST, status);

    }

    //Invalid JSON
    @Test
    public void processRequestInvalidJSONTest() {
        ResponseEntity<ResponseObj> response = server
                .withBasicAuth("applicationUser", "applicationPassword")
                .postForEntity(endpoint, "sdfsdfds", ResponseObj.class);

        HttpStatus status = response.getStatusCode();
        assertEquals("Invalid response from server : " + response, HttpStatus.BAD_REQUEST, status);
    }

    /*
        Helper functions to create the request payload
     */

    private RequestObj buildRequestBody() {
        return new RequestObj(UUID.randomUUID().toString(), "ndavid@us.ibn.com", Arrays.asList(new Person("Nigel")));
    }

    private RequestObj buildBadRequestBody() {
        return new RequestObj(UUID.randomUUID().toString(), "ndavid@us.ibn.com", null);
    }

}
